var port = 8000;
var app = require('http').createServer(handler),
  io = require('socket.io').listen(app),
  fs = require('fs'),
  sys = require('util'),
  exec = require('child_process').exec,
  child, child1;
//Escuchamos en el puerto 8000
app.listen(port);
//Si todo va bien al abrir el navegador, cargaremos el archivo index.html
function handler(req, res) {
	fs.readFile(__dirname+'/index.html', function(err, data) {
		if (err) {
      //Si hay error, mandaremos un mensaje de error 500
			console.log(err);
			res.writeHead(500);
			return res.end('Error loading index.html');
		}
		res.writeHead(200);
		res.end(data);
	});
}
 
//Cuando abramos el navegador estableceremos una conexión con socket.io.
//Cada X segundos mandaremos a la gráfica un nuevo valor. 
io.sockets.on('connection', function(socket) {
	var sensor0 = 0, sensor1 = 0, sensor2 = 0, angle0 = 0, angle1 = 0, sendData = 1, sendData2 = 1;


//Función para asignar sensores
setInterval(function(){

	//SensorA
    child = exec("cat /home/Sensor0", function (error, stdout, stderr) {
    if (error == null) {
	sensor0 = parseFloat(stdout);
    } else {
	sendData = 0;
	console.log('exec error: ' + error);
    }
  });

	//SensorB
    child = exec("cat /home/Sensor1", function (error, stdout, stderr) {
    if (error == null) {
	sensor1 = parseFloat(stdout);
    } else {
	sendData = 0;
	console.log('exec error: ' + error);
    }
  });

	//SensorC
    child = exec("cat /home/Sensor2", function (error, stdout, stderr) {
    if (error == null) {
	sensor2 = parseFloat(stdout);
    } else {
	sendData = 0;
	console.log('exec error: ' + error);
    }
  });
    
if (sendData == 1) {
      socket.emit('Sensors', sensor0, sensor1, sensor2); 
    } else {
      sendData = 1;
    }
  }, 500);

setInterval(function(){

	//Angulo azimulal
    child1 = exec("cat /home/pos0", function (error, stdout, stderr) {
    if (error == null) {
	angle0 = parseFloat(stdout);
    } else {
	sendData2 = 0;
	console.log('exec error: ' + error);
    }
  });

	//Angulo inclinación
    child1 = exec("cat /home/pos1", function (error, stdout, stderr) {
    if (error == null) {
	angle1 = parseFloat(stdout);
    } else {
	sendData2 = 0;
	console.log('exec error: ' + error);
    }
  });

if (sendData == 1) {
      socket.emit('Angles', angle0, angle1); 
    } else {
      sendData2 = 1;
    }
  }, 500);

});
