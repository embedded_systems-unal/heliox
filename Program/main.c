/*
 * main.c
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 	
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "HellioXControl.h"

int main (void)
{
	int data0[1], data1[1], data2[1];
	int step0='B';		//initial step for Stepper0
	int step1='C';
	char out0[0], out1[0];
	float count0=0;
	float count1=0;
	FILE *position0;
	FILE *position1;
	
	Init();
	
	while(1)
	{
		SpiRead(0, data0);
		SpiRead(1, data1);
		SpiRead(2, data2);
		printf("ADC0 = %i \t", data0[0]);
		printf("ADC1 = %i \t", data1[0]);
		printf("ADC2 = %i \n", data2[0]);
		
		//move clockwise
		StepperSelect('0');
		if(data2[0]-2 > data1[0])
		{
			StepperMove(step0, '0', out0);
			step0=out0[0];
			count0=count0-0.3644;
			printf("data2 << data1\n");
		}
		else
		{
			if(data2[0]+2 < data1[0])
			{
				StepperMove(step0, '1', out0);
				step0=out0[0];
				count0=count0+0.3644;
				printf("data2 << data1\n");	
			}
			else
			{
				printf("==");
			}
		}
		position0 = fopen("pos0", "w");
		fprintf(position0, "%f\n", count0);
		fclose(position0);
		
		//move panel
		StepperSelect('1');
		if(data0[0]-2 > data1[0])
		{
			StepperMove(step1, '1', out1);
			step1=out1[0];
			count1=count1+0.07627;
			printf("data1 << data0\n");
		}
		else
		{
			if(data0[0]+2 < data1[0])
			{
				StepperMove(step1, '0', out1);
				step1=out1[0];
				count1=count1-0.07627;
				printf("data1 >> data0\n");	
			}
			else
			{
				printf("=\n");
			}
		}
		position1 = fopen("pos1", "w");
		fprintf(position1, "%f\n", count1);
		fclose(position1);
		
		printf("pos0 %f \t pos1 %f \n", count0, count1);
	}
}
