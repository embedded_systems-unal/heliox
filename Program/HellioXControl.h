/*
 * SpiRead.h
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
*/

#ifndef HELLIOXCONTROL_H
#define HELLIOXCONTROL_H

void Init(void);

//Reads SPI from TLC548
//http://www.ti.com/lit/ds/symlink/tlc548.pdf
void SpiRead(char chip, int *data);

//Control for DRV8803 stepper driver 
//http://www.ti.com/lit/ds/symlink/drv8803.pdf
void StepperSelect(char stepper);
void StepperMove(char step, char dir, char *out);

#endif

