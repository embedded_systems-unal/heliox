#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <string.h>
#include <time.h>
#include "gpio-mmap.h"

int flag=0;

//############################################################
//############## INITIALIZE OLIMEX PORTS  #####################
void Init(void)
{
	gpio_map();
	
	//Spi
	gpio_output(0, 5);	//cs0 PIN[9] GPIO[5]
	gpio_output(0, 6);	//cs1 PIN[7] GPIO[6]
	gpio_output(0, 7);	//cs2 PIN[5] GPIO[7]
	gpio_output(0, 16);	//clk PIN[3] GPIO[16]
	gpio_input(0, 4);	//data PIN[11] GPIO[4]
	
	//Stepper
	gpio_output(0, 36);	//stepper0 PIN[27] GPIO[36]
	gpio_output(0, 37);	//stepper1 PIN[25] GPIO[37]
	gpio_output(0, 32);	//A PIN[35] GPIO[32]
	gpio_output(0, 33);	//B PIN[33] GPIO[33]
	gpio_output(0, 34);	//C PIN[31] GPIO[34]
	gpio_output(0, 35);	//D PIN[29] GPIO[35]
	GPIO_WRITE_PIN(36, 1);
	GPIO_WRITE_PIN(37, 1);
	GPIO_WRITE_PIN(32, 0);
	GPIO_WRITE_PIN(33, 0);
	GPIO_WRITE_PIN(34, 0);
	GPIO_WRITE_PIN(35, 0);
}

//###########################################################
//################  READ SENSORS  ############################
void SpiRead(char chip, int *data)
{
	int j, cs;
	unsigned int buff[7];
	char filename[7];
	int intens;
	FILE *string;
	
	switch(chip)
	{
		case 0:		//cs Sensor0
		{cs = 5;
		strcpy(filename, "Sensor0");}
		break;
		case 1:		//cs Sensor1
		{cs = 6;
		strcpy(filename, "Sensor1");}
		break;
		case 2:		//cs Sensor2
		{cs = 7;
		strcpy(filename, "Sensor2");}
		break;
		default :	//error choosing sensor
		{printf("Sensor from 0 to 2 \n");}
		break;
	}
	usleep(200); // minimal convertion time
	GPIO_WRITE(0, cs, 0);
	for(j = 0; j < 8; j++)
	{
		usleep(100);
		GPIO_WRITE(0, 16, 1);		//clk rising edge
		buff[j] = GPIO_READ(0, 4);	//read data
		usleep(100);
		GPIO_WRITE(0, 16, 0);		//clk falling edge
	}
	//convert data to int
	data[0] = 128*buff[0]+64*buff[1]+32*buff[2]+16*buff[3]+8*buff[4]+4*buff[5]+2*buff[6]+1*buff[7];
	intens=-data[0]+255;
	
	//write data to files
	string = fopen(filename, "w");
	fprintf(string, "%d\n", intens);
	fclose(string);
	
	GPIO_WRITE(0, 5, 1);	//re-inti cs0
	GPIO_WRITE(0, 6, 1);	//re-inti cs1
	GPIO_WRITE(0, 7, 1);	//re-inti cs2
}

//##############################################################
//################  STEPPER SELECTION  ##########################
void StepperSelect(char stepper)
{
	switch(stepper)
	{
		case 48:		//Select Stepper 0
		{
			GPIO_WRITE_PIN(36, 0);
			GPIO_WRITE_PIN(37, 1);
			flag=0;
		}
		break;
		case 49:		//Select Stepper 1
		{
			GPIO_WRITE_PIN(37, 0);
			GPIO_WRITE_PIN(36, 1);
			flag=1;
		}
		break;
		default :
		{printf("Choose Stepper 0 or 1 \n");}
		break;
	}
}

//##########################################################
//#################  STEPPER MOVE  ##########################
void StepperMove(int step, char dir, char *out)
{
	int pin;
	switch(dir)
	{
		case 48:		//Stepper Positive Direction
		{	
			if(step==68)
				{out[0]=step-3;}
			else
				{out[0]=step+1;}
		}
		break;
		case 49:		//Stepper Negative Direction
		{			
			if(step==65)
				{out[0]=step+3;}
			else
				{out[0]=step-1;}
		}
		break;
		default :
			{printf("Choose Direction 0 or 1 \n");}
		break;
	}
	pin=out[0]-33;
	GPIO_WRITE_PIN(pin, 1);
	if(flag == 0)
		{usleep(25000);}	//25 ms for big stepper
	else
		{usleep(5000);}		//5 ms for small stepper
	GPIO_WRITE_PIN(pin, 0);
}
